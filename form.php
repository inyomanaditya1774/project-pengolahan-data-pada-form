<?php
if(isset($_POST['submit'])) {
    $nama = $_POST['nama'];//mengambil inputan dan mendefinisikannya menjadi $nama
    $mapel = $_POST['mapel'];//mengambil inputan dan mendefinisikannya menjadi $mapel
    $uts = $_POST['uts'];//mengambil inputan dan mendefinisikannya menjadi $uts
    $uas = $_POST['uas'];//mengambil inputan dan mendefinisikannya menjadi $uas
    $tgs = $_POST['tgs'];//mengambil inputan dan mendefinisikannya menjadi $tgs
    
    //Mencari nilai akhir
    $nilai_akhir = $uts * 0.35 + $uas * 0.5 + $tgs * 0.15; //menghitung nilai dengan perbandingan

    //Menampilkan Grade dari nilai akhir
    if ($nilai_akhir>=90) {
        $grade = "A";
    } elseif ($nilai_akhir>=70) {
        $grade = "B";
    } elseif ($nilai_akhir>=50) {
        $grade = "C";
    } else{
        $grade = "D";
    }
}
?>
<Doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>FORM NILAI</title>
  </head>
  <body>
    <div class="container">
        <h2 class="alert alert-info text-center mt-3">FORM NILAI SISWA</h2><!-- memberikan warna danmembuat agar konten tidak mengisi seluruh layar dan berposisi ditengah -->
        <form action ="" method="POST"><!-- membuat form dengan metode post dan tidak menambahkan action karena pemrosesannya dalam file yang sama -->
        <div class="col-12 mb-3"><!-- membuat form yang berukuran 12 dalam row dengan margin bottom 3 -->
                <label for="nama" class="form-label">Nama</label><!-- membuat label diluar kolom dengan nama Nama -->
                <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukkan Nama Siswa"><!-- Mendefinisikan type dari inputan berupa text dengan name nama dan id nama -->
            </div>
            <div class="col-12 mb-3">
                <label for="mapel" class="form-label">Mata Pelajaran</label>
                <select name="mapel" class="form-control" id="mapel" >
                    <option selected>Silahkan Pilih Mata Pelajaran</option>
                    <option value="MATEMATIKA">Matematika</option>
                    <option value="BAHASA INDONESIA">Bahasa Indonesia</option>
                    <option value="BAHASA INGGRIS">Bahasa Inggris</option>
                    <option value="ILMU PENGETAHUAN ALAM">Ilmu Pengetahuan Alam</option>
                    <option value="ILMU PENGETAHUAN SOSIAL">Ilmu Pengetahuan Sosial</option>
                </select>
            </div><!-- Membuat form dalam bentuk type inputam select  -->
            <div class="col-12 mb-3">
                <label for="uts" class="form-label">Nilai UTS</label>
                <input type="number" name="uts" class="form-control" id="uts" placeholder="Masukkan Nilai UTS">
            </div><!-- Membuat form dalam bentuk type inputan number -->
            <div class="col-12 mb-3">
                <label for="uas" class="form-label">Nilai UAS</label>
                <input type="number" name="uas" class="form-control" id="uas" placeholder="Masukkan Nilai UAS">
            </div><!-- Membuat form dalam bentuk type inputan number -->
            <div class="col-12 mb-3">
                <label for="tsg" class="form-label">Nilai Tugas</label>
                <input type="number" name="tgs" class="form-control" id="tgs" placeholder="Masukkan Nilai Tugas">
            </div><!-- Membuat form dalam bentuk type inputan number -->
            <button type="submit" name="submit"  class="btn btn-dark justify-content-center">Submit</button>
        </form>  
        </div><!-- Membuat tombol subbmit yang akan memroses dari php-->

        <div class="col-md-6" >
            <br>
            <br>
            <?php if (isset($_POST['submit'])) : ?> 
<!-- mengambil hasil dari proses yang telah dijalankan -->
            <div class="row justify-content-center">
            <div class="col-8 border rounded-2  mt-8 p-8">
            <div class="alert alert-primary" >

                Nama                :<?php echo $nama ?> <br>
                Mata Pelajaran      :<?php echo $mapel ?> <br>
                Nilai UTS           :<?php echo $uts ?> <br>
                Nilai UAS           :<?php echo $uas ?> <br>
                Tugas               :<?php echo $tgs ?> <br>
                Grade               :<?php echo $grade ?> <br>
            </div>
            </div>
        </div>
        <?php endif ; ?>
        </div>
    </div>

        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
  </body>
</html>